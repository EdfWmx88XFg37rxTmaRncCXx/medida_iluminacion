#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "GPIO_LPC17XX.h"
#include "PIN_LPC17XX.h"
#include <stdio.h>
#include "rebotes.h"
#include "tsl2591_light.h"
#include "SPI_LPC17XX.h"
#include "LPC17xx.h" 
#include "RTE_Device.h"
#include "Driver_SPI.h"
#include "lcd.h"

void ledPWM(void);	
void relojSistema(void);
void blink_LED1(int cadencia);
void initLEDS(void);
int brillo_LED1 = 10;
int suma = 0;



///CONFIGURACION DE LEDS RGB////
#define PUERTO_RGB 2
#define PIN_LED_AZUL 1
#define PIN_LED_ROJO 3
#define PIN_LED_VERDE 2
/////FIN CONFIGURACION LEDS RGB//

#define PUERTO_LEDS 1
#define LED_1 18
#define LED_2 20
#define LED_3 21
#define LED_4 23



//Mensaje para la USART
char textoA[50]; //mensaje para 
char textoB[50];
char msj[80];


///variables reloj//
int decimas = 0;
int segundos= 0;
int minutos = 27;
int horas = 13;

int segundosD;
int minutosD;
int horasD;



void timerOS (void const *argument);
osTimerId timer_callback;
osTimerDef (periodic, timerOS);



///HILO AUTOMATA////
typedef enum {INACTIVO, ACTIVO, MEDIDA} t_estados;  /////MAQ.ESTADOS///
t_estados estadoSistema;


//////DECLARACION HILOS///////////
void automata (void const *argument);
osThreadId hilo_automata;
osThreadDef (automata, osPriorityNormal, 1, 0);

//Hilo LCD
void DisplayLCD(void const *argument);                             
osThreadId LCD_hilo;                                          
osThreadDef (DisplayLCD, osPriorityNormal, 1, 600); 



                         


int Init_Thread (void){
	Init_Antirrebotes();
	
	timer_callback = osTimerCreate(osTimer(periodic), osTimerPeriodic, (void *) 0);
	osTimerStart(timer_callback, 500);
	LCD_hilo = osThreadCreate (osThread(DisplayLCD), NULL);
	
	
	hilo_automata = osThreadCreate(osThread(automata), NULL);
	estadoSistema = INACTIVO;
	
	if(!hilo_automata){
		return -1;
	}else{
		return 0;
	}	
	
}

void timerOS (void const *argument){
	decimas++;
	ledPWM();
	relojSistema();
			
}


	

void initLEDS(void){
	GPIO_SetDir(PUERTO_LEDS,LED_1,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_RGB,PIN_LED_VERDE,GPIO_DIR_OUTPUT);
	LPC_PINCON -> PINSEL4 = (1<<4)|(0<<5);
	LPC_PWM1 -> PR = 0x00;
}	


void automata (void const *argument){
	initLEDS();
	while(1){
		
	
		switch (estadoSistema){


			case(INACTIVO):
				osSignalSet(LCD_hilo, 0x01);

				break;
			
			case(ACTIVO):
				blink_LED1(1000);
				osSignalSet(LCD_hilo, 0x20);
			
				
						
						
				break;

			case(MEDIDA):

				break;
		}
	}osThreadYield();
}

void DisplayLCD(void const *argument) {		
	LCD_init();
  while (1) {
		osEvent evento = osSignalWait(0x00, osWaitForever);
		
		switch( evento.value.signals){
			case 0x01:
					LCD_clear();
					osDelay(1);
					sprintf( textoA, "%s%02d%s%02d%s%02d", "HORA:", horas,":",minutos,":",segundos);
					sprintf( textoB,"%s", "Una Prueba BRO");
					LCD_Write(textoA,1);
					LCD_Write(textoB,2);
					break;
			
			case 0x20:
					LCD_clear();
					osDelay(1);
					sprintf( textoA, "%s%02d%s%02d%s%02d", "NORMAL:", horas,":",minutos,":",segundos);
					sprintf( textoB,"%s", "OTRA Prueba BRO");
					LCD_Write(textoA,1);
					LCD_Write(textoB,2);
					break;
				
		}
		
	}

}



void relojSistema(void){
	if(decimas > 19){
		decimas = 0;
		 segundos++;
	 }
	
	if(segundos > 59){
		 segundos = 0;
		 minutos++;
	 }
	 
	 if(minutos > 59){
	  minutos = 0;
		horas++;
	 }

	 if(horas > 23){
		 horas =0;
	 }		
}



void ledPWM(void){	
	if(suma){
			brillo_LED1 += 5;
		}else{
			brillo_LED1-= 5;
		}	
		
		if(brillo_LED1 >= 90){
			suma = 0;
		} else if(brillo_LED1 <= 10){
			suma = 1;
		}
			LPC_PWM1 -> PR = 0x00;
			LPC_PWM1 ->MCR= (1<<1);
			LPC_PWM1 -> MR0 = 100;
			LPC_PWM1 ->MR3 = brillo_LED1;
			LPC_PWM1 -> LER = (1<< 0) | (1<<3);
			
			LPC_PWM1 -> PCR = (1<<11);
			LPC_PWM1 -> TCR = (1<<0) | (1<<3);
	}


void blink_LED1(int cadencia){
	
	GPIO_PinWrite(PUERTO_LEDS,LED_1,1);
	osDelay(1000);
	GPIO_PinWrite(PUERTO_LEDS,LED_1,0);
osDelay(1000);	
}	



void EINT3_IRQHandler (void){

/*	
	Gesti?n para los flancos de subida para las interrupciones producidas por los pulsadores del Joystick
*/ 

 
  if (LPC_GPIOINT-> IO0IntStatR & 1 << UP){ 
		LPC_GPIOINT->IO0IntClr |=(1 <<UP);
		LPC_GPIOINT->IO0IntEnR &= ~(1 << UP);
		osSignalSet(Hilo_Antirrebotes, ARRIBA);
		
		if(estadoSistema == INACTIVO){
			estadoSistema = ACTIVO;
		}else if(estadoSistema == ACTIVO) {
			estadoSistema = INACTIVO;
		}
		
 }
  
 else if (LPC_GPIOINT-> IO0IntStatR & 1 << DOWN){   
  LPC_GPIOINT->IO0IntClr|=(1 <<DOWN);
  LPC_GPIOINT->IO0IntEnR &= ~(1 << DOWN);
  osSignalSet(Hilo_Antirrebotes, ABAJO);
	if(estadoSistema == ACTIVO) {
			estadoSistema = MEDIDA;
		}
	 		 
 }


 if (LPC_GPIOINT-> IO0IntStatF & 1 << UP)
  osSignalSet(Hilo_Antirrebotes, ARRIBA);
 else if (LPC_GPIOINT-> IO0IntStatF & 1 << DOWN)
  osSignalSet(Hilo_Antirrebotes, ABAJO);

 
  //tempGuarda = guarda_1d(tempGuarda);
 
LPC_GPIOINT->IO0IntClr=(1 <<UP | 1 <<DOWN ); //se limpian todos los posibles flags residuos que se hayan produido
}
