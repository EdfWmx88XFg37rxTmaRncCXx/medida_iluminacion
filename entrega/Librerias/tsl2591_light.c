#include "tsl2591_light.h"



uint8_t ENA_REGISTER =0xA0;		//direccion del registro enable
uint8_t ENA_CONFIG =0X13;	//Habilitamos 3 bit  
uint8_t CONTROL_REGISTER =0xA1;			//direecion del registro control
uint8_t CONTROL_CONFIG  =0x12	;	//establecemos la ganacia y la integral de 300ms
uint8_t CH0_LOW = 0xB4;		//direecion del canal 0 low
uint8_t CH1_LOW = 0xB6;		//dirreccion del canal 1 low


uint8_t data_recive_ch0_low[1];
uint8_t data_recive_ch1_low[1];

 int cp1= 0;
 float luz=0 ,lux= 0;
 uint32_t luminosidad = 0;


void TSL2591_init(void){
		I2Cdrv ->Initialize (NULL);
		I2Cdrv->PowerControl(ARM_POWER_FULL);
		I2Cdrv->Control(ARM_I2C_BUS_SPEED, ARM_I2C_BUS_SPEED_FAST);
		I2Cdrv->Control(ARM_I2C_BUS_CLEAR, 0);

		I2Cdrv->MasterTransmit (I2C_ADDR,&ENA_REGISTER, 1, true);
		while (I2Cdrv->GetStatus().busy){}
		I2Cdrv ->MasterTransmit(I2C_ADDR,&ENA_CONFIG,1,true);
		while (I2Cdrv->GetStatus().busy){}
				
		I2Cdrv->MasterTransmit(I2C_ADDR,&CONTROL_REGISTER,1,true);			//Registro addr 0x01
		while(I2Cdrv->GetStatus().busy){}
		I2Cdrv->MasterTransmit(I2C_ADDR,&CONTROL_CONFIG,1,false);		//comando cmd 0x12
		while(I2Cdrv->GetStatus().busy){}
}	

	
void TSL2591_tomarMuestra(void){		//lectura de ch0 y ch1
			
		I2Cdrv->MasterTransmit(I2C_ADDR,&CH0_LOW,1,true);					//0x14
		while(I2Cdrv->GetStatus().busy){}
		I2Cdrv->MasterReceive(I2C_ADDR, data_recive_ch0_low,1, false);		//CH0_LOW
		while(I2Cdrv->GetStatus().busy){}

		//Lectura del canal 1 LOW
		I2Cdrv->MasterTransmit(I2C_ADDR,&CH1_LOW,1,true);					
		while(I2Cdrv->GetStatus().busy){}
		I2Cdrv->MasterReceive(I2C_ADDR,data_recive_ch1_low,1, false);	
		while(I2Cdrv->GetStatus().busy){}

		cp1=(300*25)/762;
		luz=(data_recive_ch0_low[0] - (1.64*data_recive_ch1_low[0])/cp1);
		lux=(luz/255)*100;	
		
		luminosidad = lux;

}

//Copiar para usar en main
//extern void TSL2591_init(void);
//extern void TSL2591_tomarMuestra(void);

