#include "LM75BD_TEMP.h"


void LM75BD_Init(void){
	I2Cdev->Initialize(NULL);
	I2Cdev->PowerControl(ARM_POWER_FULL);
	I2Cdev->Control(ARM_I2C_BUS_SPEED,ARM_I2C_BUS_SPEED_FAST);
	I2Cdev->Control(ARM_I2C_OWN_ADDRESS,LM75B_ADDRESS);
}

float LM75BD_leerTemp(void){
	uint8_t cmd[2];
	uint8_t buff[2];
	
	cmd[0] = 0x01; //REG_CONF
	cmd[1] = 0x00;
	I2Cdev->MasterTransmit(LM75B_ADDRESS,cmd,2,false);
	while(I2Cdev->GetStatus().busy);
	cmd[0] = 0x00; //REG_TEM
	
	float temperatura=0;
	I2Cdev->MasterTransmit(LM75B_ADDRESS,cmd,1,true);
	while(I2Cdev->GetStatus().busy);
	I2Cdev->MasterReceive(LM75B_ADDRESS,buff,2,false);
	while(I2Cdev->GetStatus().busy);
	
	temperatura = ((buff[0]<<8 | buff[1])/256.0);
	/*
		Podria evaluar el signo de la temperatura ( temperatura >> 10 == 1 ---> negativo)
	*/
	return temperatura;
}
