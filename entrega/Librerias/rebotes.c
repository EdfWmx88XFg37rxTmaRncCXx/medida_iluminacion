#include "rebotes.h"

void quitarRebotes (void const *argument); 
osThreadId  Hilo_Antirrebotes;
osThreadDef (quitarRebotes, osPriorityNormal, 1, 0);


int Init_Antirrebotes(void) {
Hilo_Antirrebotes = osThreadCreate (osThread(quitarRebotes), NULL);
configPulsadores(); 
	
  if (!Hilo_Antirrebotes) return(-1);  
  return(0);
}

/*
Gestión para eliminar rebotes de un pulsador. Se incluyen todos los pulsadores del Joystick de MBED
*/ 

void quitarRebotes (void const *argument) {

  osEvent evento;
  bool flancoBajada_dcha = false;
  bool flancoBajada_izqda = false; 
  bool flancoBajada_arriba = false; 
  bool flancoBajada_abajo = false;
  bool flancoBajada_central = false;	
	
  while (1) {
		
	evento = osSignalWait(0, osWaitForever);
  osDelay(145); //espera hasta la proxima interrupcion
		
/*
	PULSADOR_CENTRAL	 
*/
    if(evento.value.signals == CENTRO){
  
      if(!flancoBajada_central){
        LPC_GPIOINT->IO0IntEnF |= (1 << CENTER) ; 
        LPC_GPIOINT->IO0IntClr |= (1 <<CENTER) ; 
        flancoBajada_central = true;
      }
      else{
        LPC_GPIOINT->IO0IntEnF &= ~(1 << CENTER) ; 
        LPC_GPIOINT->IO0IntEnR |= (1 << CENTER) ; 
        flancoBajada_central=false;
      }
     } 

/*
PULSADOR_IZQUIERDA		 
*/

    if(evento.value.signals == IZQUIERDA){
  
      if(!flancoBajada_izqda){
        LPC_GPIOINT->IO0IntEnF |= (1 << LEFT) ; 
        LPC_GPIOINT->IO0IntClr |= (1 <<LEFT) ; 
        flancoBajada_izqda = true;
      }
      else{
        LPC_GPIOINT->IO0IntEnF &= ~(1 << LEFT) ; 
        LPC_GPIOINT->IO0IntEnR |= (1 << LEFT) ; 
        flancoBajada_izqda=false;
      }

     }
/*
PULSADOR_ARRIBA		 
*/
    if(evento.value.signals == ARRIBA){
  
      if(!flancoBajada_arriba){
        LPC_GPIOINT->IO0IntEnF |= (1 << UP) ; 
        LPC_GPIOINT->IO0IntClr |= (1 <<UP) ; 
        flancoBajada_arriba = true;
      }
      else{
        LPC_GPIOINT->IO0IntEnF &= ~(1 << UP) ; 
        LPC_GPIOINT->IO0IntEnR |= (1 << UP) ; 
        flancoBajada_arriba=false;
      }
     }	
		 
/*
PULSADOR_DERECHA		 
*/

    if(evento.value.signals == DERECHA){
  
      if(!flancoBajada_dcha){
        LPC_GPIOINT->IO0IntEnF |= (1 << RIGHT) ; 
        LPC_GPIOINT->IO0IntClr |= (1 <<RIGHT) ; 
        flancoBajada_dcha = true;
      }
      else{
        LPC_GPIOINT->IO0IntEnF &= ~(1 << RIGHT) ; 
        LPC_GPIOINT->IO0IntEnR |= (1 << RIGHT) ;
        flancoBajada_dcha=false;
      }
     } 		 
		 
		 
/*
PULSADOR_ABAJO		 
*/

    if(evento.value.signals == ABAJO){  
      if(!flancoBajada_abajo){
        LPC_GPIOINT->IO0IntEnF |= (1 << DOWN) ; 
        LPC_GPIOINT->IO0IntClr |= (1 <<DOWN) ; 
        flancoBajada_abajo = true;
      }
      else{
        LPC_GPIOINT->IO0IntEnF &= ~(1 << DOWN) ; 
        LPC_GPIOINT->IO0IntEnR |= (1 << DOWN) ; 
        flancoBajada_abajo=false;
      }
     }
     
  }
  
}



void configPulsadores(void){ //configuración de pulsadores
	
	PIN_Configure(PUERTO_JOYSTICK,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  PIN_Configure(PUERTO_JOYSTICK,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);  
	PIN_Configure(PUERTO_JOYSTICK,CENTER,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JOYSTICK,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  PIN_Configure(PUERTO_JOYSTICK,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	LPC_GPIOINT->IO0IntEnR =  1 << DOWN | 1 << UP | 1 << LEFT | 1 << RIGHT | 1 << CENTER ;
	NVIC_EnableIRQ(EINT3_IRQn);
	
}
