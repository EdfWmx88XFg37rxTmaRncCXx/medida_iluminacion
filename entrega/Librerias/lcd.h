/*
Librer�a para manejar el LCD SPI 128x32 incluido en la mbed_AppBoard
*Fuente utilizada, Arial 12x12
*
*LCD_init(); --> secuencia de arranque del display
*LCD_clear(); --> borra los datos presentados en la pantalla
*LCD_Write(char[] texto, int linea); --> escribe en la pantalla LCD una cadena de String
*--se pasa argumentos con sprintf( textoA, "%s%u", " Prueba valor1: ", val1);
*/




#ifndef _LCD_H_
#define _LCD_H_

#include "LPC17xx.h"      
#include "GPIO_LPC17xx.h" 
#include "PIN_LPC17xx.h"
#include "RTE_Device.h"
#include "SPI_LPC17xx.h"
#include "Driver_SPI.h"


#define PUERTO 0
#define A0     6  //PIN8
#define RESET  8  //PIN6
#define CS     18 //PIN11


extern ARM_DRIVER_SPI Driver_SPI1;



void init(void);
void retardo(uint32_t microsegundos);
void reset(void);
void wr_data(unsigned char data);
void wr_cmd(unsigned char cmd);
void LCD_reset(void);
void copy_to_lcd(void);
int EscribeLetra_L1(uint8_t letra);
int EscribeLetra_L2(uint8_t letra);
void escribirLinea(uint8_t texto, int linea);
void LCD_Write(char texto[], int linea);
void LCD_init(); 
void LCD_clear();
void LCD_PintarBarraHorizontal(int posY, int linea);
void LCD_PintarBarraVertical(int posX, int linea);


extern char buffer[512];
extern char buffer2[512];
extern uint16_t posicionL1;
extern uint16_t posicionL2;
void LCD_off(void);
void LCD_on(void);
void LCD_InvertirPantalla(void);
void LCD_PintarImagenF(void);
void LCD_PintarImagenR(void);
void LCD_PintarImagenL(void);
	

	



#endif

