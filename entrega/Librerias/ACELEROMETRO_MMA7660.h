#include "Driver_I2C.h"              // ::CMSIS Driver:I2C
#include "LPC17xx.h"                    // Device header
#include "I2C_LPC17xx.h"

#define MMA7660_I2C_ADDR 76
#define reg_mode 7
#define reg_tilt 3


extern ARM_DRIVER_I2C Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdev = &Driver_I2C2;

void Init_Acelerometro();
int Leer_Acelerometro(void);