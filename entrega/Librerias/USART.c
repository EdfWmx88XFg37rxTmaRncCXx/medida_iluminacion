#include "USART.h"

char data[8];
void USART_Callback(uint32_t event){
}

void Init_USART(void){
	
	USARTdrv->Initialize(USART_Callback);
	USARTdrv->PowerControl(ARM_POWER_FULL);
	
	USARTdrv->Control(ARM_USART_MODE_ASYNCHRONOUS |
										ARM_USART_DATA_BITS_8 |
										ARM_USART_PARITY_NONE |
										ARM_USART_STOP_BITS_1 |
										ARM_USART_FLOW_CONTROL_NONE, 9600);
	
	USARTdrv->Control(ARM_USART_CONTROL_TX,1);
	USARTdrv->Control(ARM_USART_CONTROL_RX,1);	
}
