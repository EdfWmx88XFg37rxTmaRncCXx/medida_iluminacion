#include "Driver_I2C.h"              // ::CMSIS Driver:I2C
#include "LPC17xx.h"                    // Device header
#include "I2C_LPC17xx.h"


#define LM75B_ADDRESS 0x48

extern ARM_DRIVER_I2C Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdev = &Driver_I2C2;

void LM75BD_Init(void);
float LM75BD_leerTemp(void);

