
#ifndef _ZUMBADOR_H_
#define _ZUMBADOR_H_

#include "cmsis_os.h"    
#include <stdio.h>
#include "lcd.h"
#include <string.h>

#define PUERTO_BUZZER 2
#define PIN_BUZZER 0



void Init_zumbador (void);

int PWM_Buzzer_ON(void);

int PWM_Buzzer_OFF(void);
	
#endif

	