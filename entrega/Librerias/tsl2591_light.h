#ifndef _tsl2591_light_H_
#define _tsl2591_light_H_


#include "Driver_I2C.h"             
#include "LPC17xx.h"                   
#include "I2C_LPC17xx.h"

#define I2C_ADDR 0x29



extern ARM_DRIVER_I2C Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdrv = &Driver_I2C2;

//Copiar para usar en main
void TSL2591_init(void);
void TSL2591_tomarMuestra(void);

#endif