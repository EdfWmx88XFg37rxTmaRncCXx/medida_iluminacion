#include "zumbador.h"


void Init_zumbador (void){  
	GPIO_SetDir(PUERTO_BUZZER, PIN_BUZZER, GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_BUZZER, PIN_BUZZER, PIN_FUNC_1, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);	
	
	LPC_PWM1 ->MCR |= (1<<1);
  LPC_PWM1 ->MR0 = 10000;
  LPC_PWM1 ->MR1 = 15000;
  LPC_PWM1 ->LER |= (1<<0 | 1<<1);
  LPC_PWM1 ->PCR |= (1<<9);
}

int PWM_Buzzer_ON(void){
	LPC_PWM1 ->TCR |= (1<<0);
}

int PWM_Buzzer_OFF(void){
	LPC_PWM1 ->TCR |= (1<<0);
}	
	

	
	