#ifndef _REBOTES_H_
#define _REBOTES_H_

#include "cmsis_os.h"                                           
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#define PUERTO_JOYSTICK  0
#define CENTER 	16
#define DOWN   	17
#define LEFT   	15
#define UP   		23
#define RIGHT   24


#define DERECHA     0x01
#define ARRIBA      0x02
#define ABAJO       0x04
#define IZQUIERDA   0x08
#define CENTRO      0x10


void configPulsadores(void);
int Init_Antirrebotes(void);
extern osThreadId  Hilo_Antirrebotes;                           

#endif
