#include "LPC17xx.h"
                    // RTOS object definitions
#include "cmsis_os.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "SPI_LPC17xx.h"
#include "Driver_SPI.h"
#include "RTE_Device.h"
#include "lcd.h"
#include "ACELEROMETRO_MMA7660.h"

                   

extern void Init_Thread (void);
extern void Init_I2C();

#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_I2C.h"

#define Puerto_leds 1
#define led1 18
#define led2 20
#define led3 21
#define led4 23

//#define MMA7660_I2C_ADDR 76
//#define reg_mode 7
//#define reg_tilt 3

uint8_t PoLa;
uint8_t BaFro;
uint8_t BaFro1;
uint8_t BaFro2; 
 
void Init_Acelerometro(){
	I2Cdev ->Initialize(NULL);
  I2Cdev ->PowerControl(ARM_POWER_FULL);
  I2Cdev ->Control(ARM_I2C_BUS_SPEED, ARM_I2C_BUS_SPEED_STANDARD);
  I2Cdev ->Control(ARM_I2C_BUS_CLEAR, 0);	// initialize CMSIS-RTOS
}	
 
 
int Leer_Acelerometro(void) { 
	uint8_t cmd[2];
	uint8_t buf[1];
	uint8_t buf2[1];
  
	GPIO_SetDir (Puerto_leds, led1, 1<<led1);
	GPIO_SetDir (Puerto_leds, led2, 1<<led2);
	GPIO_SetDir (Puerto_leds, led3, 1<<led3);
	GPIO_SetDir (Puerto_leds, led4, 1<<led4);
	
	cmd[0]=reg_mode;
	cmd[1]=0x01;
	
	I2Cdev->MasterTransmit (MMA7660_I2C_ADDR, cmd, 2, false);
  while (I2Cdev->GetStatus().busy){}
		
	LCD_init();	
	while(1){
		cmd[0] = reg_tilt;
		I2Cdev->MasterTransmit (MMA7660_I2C_ADDR, cmd, 1, true);
		while (I2Cdev->GetStatus().busy){}
			
		I2Cdev->MasterReceive (MMA7660_I2C_ADDR, buf, 1, false);
    while (I2Cdev->GetStatus().busy){}
			
			buf2[0]= buf[0]; //	
			
			PoLa = (buf[0] << 3) >> 5;
			BaFro1 = buf2[0] << 6;
			BaFro2 = BaFro1 >> 6;			
			
			
			GPIO_PinWrite(Puerto_leds, led1, 0);
			GPIO_PinWrite(Puerto_leds, led2, 0);
			GPIO_PinWrite(Puerto_leds, led3, 0);
			GPIO_PinWrite(Puerto_leds, led4, 0);
			
			
			switch(BaFro2){
				case 2: GPIO_PinWrite(Puerto_leds, led1, 1); 
								GPIO_PinWrite(Puerto_leds, led2, 1);
								GPIO_PinWrite(Puerto_leds, led3, 1);
								GPIO_PinWrite(Puerto_leds, led4, 1);
								return 2;
											
				break;
				case 1:				
								
								switch(PoLa){
										case 1: GPIO_PinWrite(Puerto_leds, led1, 1); 														
														return 11;
														break;	
														
										case 5: GPIO_PinWrite(Puerto_leds, led2, 1); 
														return 15;
														break;
									break;
										case 2: GPIO_PinWrite(Puerto_leds, led3, 1);
														return 12;
												break;
										case 6: GPIO_PinWrite(Puerto_leds, led4, 1);
														return 16;
														break;				
											default: break;		
								}	
				
				break;
								
				default: break;		
			}
		}
	 
}